var moment = require('moment');

// Aprimorar algoritmo para cálculo de horas, usando o PDF
/* pdfJs.getDocument('relatorio.pdf').then(pdf => {
  console.debug(pdf);d
})
.catch(error => {
  console.error(error);
}); */

var horas = [
"01/11 QUI 3099 08:00 12:29 14:20 21:40",
"02/11 SEX 9997 Feriado",
"03/11 SAB 1092 18:09 22:57",
"04/11 DOM 9999 DSR",
"05/11 SEG 3099 08:23 12:03 14:32 18:16",
"06/11 TER 3099 10:05 12:14 14:01 18:46",
"07/11 QUA 3099 08:00 08:01 15:01 19:12",
"08/11 QUI 3099 09:20 12:22 14:38 18:32",
"09/11 SEX 3099 Compensação Dia",
"10/11 SAB 1092 Compensação Dia",
"11/11 DOM 9999 DSR",
"12/11 SEG 3099 08:00 12:14 14:02 19:16",
"13/11 TER 3099 08:00 08:01 13:51 19:27",
"14/11 QUA 3099 08:00 08:01 14:54 18:35",
"15/11 QUI 9997 Feriado",
"16/11 SEX 3099 09:09 12:20 14:07 17:38",
"17/11 SAB 1092 Compensação Dia",
"18/11 DOM 9999 DSR",
"19/11 SEG 3099 Compensação Dia",
"20/11 TER 9997 Feriado",
"21/11 QUA 3099 08:22 11:36 14:13 19:15",
"22/11 QUI 3099 08:00 08:01 14:07 18:55",
"23/11 SEX 3099 09:43 12:11 14:05 18:36",
"24/11 SAB 1092 Compensação Dia",
"25/11 DOM 9999 DSR",
"26/11 SEG 3099 08:00 08:01 15:08 19:50",
"27/11 TER 3099 10:10 12:20 14:37 18:45",
"28/11 QUA 3099 08:00 08:01 09:34 11:53",
"29/11 QUI 3099 10:37 12:01 13:36 18:22",
"30/11 SEX 3099 08:00 08:01 14:20 18:13"
];


for (let i = 0; i < horas.length; i++) {
  let part;
  let aux;
  part = horas[i].split(" ");
  aux = part[3];
  if (aux !== "Compensação" && aux !== "Feriado" && aux !== "DSR" && aux !== "Atestado") {
    if (part[1] === 'SAB') {
      printResult(part[0], calcularIntervalo(part[3], part[4]))
    } else {
      printResult(part[0], calcularIntevaloComAlmoco(part[3], part[4], part[5], part[6]));
    }
  }
}

function calcularIntevaloComAlmoco(inicio, inicioA, fimA, fim) {
  return calcularIntervalo(inicio, inicioA) + calcularIntervalo(fimA, fim);
}

function calcularIntervalo(inicio, fim) {
  return moment(fim, "HH:mm").valueOf() - moment(inicio, "HH:mm").valueOf();
}

function printResult(part, result) {
  console.log(part, " => ", moment.duration(result).asHours().toFixed(2));
}